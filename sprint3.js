// Get the card element
const card = document.getElementById('travelCard');

// Hover effect
card.addEventListener('mouseenter', function() {
  this.style.transform = 'translateY(-10px)';
});

card.addEventListener('mouseleave', function() {
  this.style.transform = 'translateY(0)';
});

