Project Brief:

The main purpose of this project is to create a travel blog website that provides a variety of information relating to travel. Our website aims to offer a diverse range of content, including destination recommendations, 
travel tips and a selection of recommended products to enhance the travel experience.

Sprint 2/Contact form: 
The focus of this form is to provide a user friendly contact form. The form asks several different, but simple questions to find out the needs of the user.
The form also makes it easier for our team to answer any questions, as the basics of their information is laid out in an easy-to-read way, Without having the need to read through a paragraph of text
https://s247599.bitbucket.io/sprint2.html 

Sprint3-Card
The focus of this sprint is to come up with a card design that can be replicated and used in the full website. As travel websites rely on the visuals of a holiday,
the card itself will predominantly feature a picture of a location, with a little extra styling when it is highlighted. When the card is clicked it will take the user to the
article associated with the location. As there currently are no articles, it will simply be a placeholder url
https://s247599.bitbucket.io/sprint3.html